package com.task584.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task584.restapi.model.CEmployee;
import com.task584.restapi.respository.IEmplyeeRespository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class EmplyeeController {
    @Autowired
    IEmplyeeRespository pEmployeeRepository;

    @GetMapping("/emplyees")
    public ResponseEntity<List<CEmployee>> getAllCDrinks() {
        try {
            List<CEmployee> listCustomer = new ArrayList<CEmployee>();
            pEmployeeRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
