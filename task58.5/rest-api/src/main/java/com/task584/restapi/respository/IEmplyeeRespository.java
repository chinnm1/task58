package com.task584.restapi.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task584.restapi.model.CEmployee;

public interface IEmplyeeRespository extends JpaRepository<CEmployee, Long> {

}
