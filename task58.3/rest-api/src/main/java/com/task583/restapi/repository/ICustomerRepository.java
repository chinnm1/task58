package com.task583.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task583.restapi.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {

}
