package com.task583.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task583.restapi.model.CCustomer;
import com.task583.restapi.repository.ICustomerRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CCustomerController {
    @Autowired
    ICustomerRepository pCustomerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCDrinks() {
        try {
            List<CCustomer> listCustomer = new ArrayList<CCustomer>();
            pCustomerRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
