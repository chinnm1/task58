package com.task584.restapi.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task584.restapi.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct, Long> {

}
