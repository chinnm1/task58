package com.task584.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task584.restapi.model.CProduct;
import com.task584.restapi.respository.IProductRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProductController {
    @Autowired
    IProductRepository pProductRepository;

    @GetMapping("/products")
    public ResponseEntity<List<CProduct>> getAllCDrinks() {
        try {
            List<CProduct> listCustomer = new ArrayList<CProduct>();
            pProductRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
